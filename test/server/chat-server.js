
var server, chatServer, client;
var assert = require('assert');
var http = require('http').Server;
var ioc = require('socket.io-client');
var ChatServer = require('../../lib/chat-server.js');

// stolen from socket.io testing suite
// creates a socket.io client for the given server
function client(srv, nsp, opts){
  if ('object' == typeof nsp) {
    opts = nsp;
    nsp = null;
  }
  var addr = srv.address();
  if (!addr) addr = srv.listen().address();
  var url = 'ws://' + addr.address + ':' + addr.port + (nsp || '');
  return ioc(url, opts);
}

describe('chat server', function () {
  beforeEach(function () {
    server = http();
    chatServer = new ChatServer(server);
  });

  describe('guestName', function (done) {
    it('emits nameResult, with an object containing the unique name of the guest', function (done) {
      var socket1 = client(server);
      var socket2 = client(server, { multiplex: false });

      socket2.on('nameResult', function (data) {
        assert(/2/.test(data.name));
        done();
      });
    });

    it('allows guests to change their name', function (done) {
      var newName = 'bob';
      var emissions = 0;
      var socket = client(server);
      socket.emit('nameAttempt', newName);

      socket.on('nameResult', function (data) {
        emissions++;
        // nameResult is called on initial connection
        // so we wait until the 2nd emit to assert
        // this may be a smell
        if (emissions === 2) {
          assert(data.name.match(newName));
          done();
        }
      });
    });
  });

  it('does not allow users to use a name that is already taken');
  it('allows user to change rooms', function () { });
  it('removes a username from the pool when the user leaves', function () { });
  it('ties a user to an account', function () { });
});
