This is an unfinished version of an example in chapter 2 of [Node.js in action](http://www.manning.com/cantelon/).

All code is licensed to the original authors.

# Run
```bash
$ npm i
$ npm start
```

# Todo

- [ ] Integration tests.
- [ ] Client side tests.
- [ ] Robust server side tests.

- Server Side
  - add authentication via github/twitter
  - keep log of chats
  - standardize "user" and "message" models
  - persistent rooms?
- Client Side
  - router routes for rooms/users/etc
  - use templates
  - remove old $.cruft
