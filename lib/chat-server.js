// load in  application config here
var debug = require('debug')('chat-app:chatServer');
var socketio = require('socket.io');
var _ = require('lodash');

module.exports = ChatServer;

// socket.io wrapper that consumes
// consume http server
// and provides chat room functionality
function ChatServer (httpServer) {
  this.io = null;
  this.guestNumber = 1;
  this.nickNames = {};
  this.namesUsed = [];
  this.currentRoom = {};
  this.defaultRoom = 'lobby';
  this.io = socketio(httpServer);

  this.listen();
  return this;
}

_.extend(ChatServer.prototype, {
  listen: function (server) {
    this.io.on('connection', this.handleClientConnection.bind(this));
  },
  assignGuestName: function (socket) {
    var name = 'Guest' + this.guestNumber;

    this.nickNames[socket.id] = name;

    socket.emit('nameResult', {
      success: true,
      name: name
    });

    this.namesUsed.push(name);
    this.guestNumber = this.guestNumber + 1;
  },
  handleMessageBroadcasting: function (socket) {
    socket.on('message', function (message) {
      debug('emitting: "' +  this.nickNames[socket.id] + ': ' + message.content + '" to room: ' + message.room);
      // 1. this currently relies on the server to
      // process all messages
      // and the client "dumbly"
      // receives it's own message
      // 2. message may be a great canidate
      // for an isomorphic model
      this.io.to(message.room).emit('message.user', {
        username: this.nickNames[socket.id],
        content: message.content
      });
    }.bind(this));
  },
  handleNameChangeAttempts: function (socket) {
    socket.on('nameAttempt', function(name) {
      if (name.indexOf('Guest') === 0) {
        socket.emit('nameResult', {
          success: false,
          message: 'Names cannot begin with Guest.'
        });
      } else {
        if (this.namesUsed.indexOf(name) === -1) {
          var previousName = this.nickNames[socket.id];
          var previousNameIndex = this.namesUsed.indexOf(previousName);
          this.namesUsed.push(name);
          this.nickNames[socket.id] = name;

          delete this.namesUsed[previousNameIndex];

          socket.emit('nameResult', {
            success: true,
            name: name
          });
        } else {
          socket.emit('nameResult', {
            success: false,
            message: 'That name is already in use'
          });
        }
      }
    }.bind(this));
  },
  handleClientConnection: function (socket) {
    // I hate the way these handlers are structured
    // I want something clean like this:
    // socket.on('foo', this.handleFoo.bind(this));
    // but the fact that the handlers need to
    // know about the socket prevents us from doing so
    this.assignGuestName(socket);
    this.handleNameChangeAttempts(socket);
    this.handleMessageBroadcasting(socket);
    this.handleRoomJoining(socket);
    this.handleClientDisconnection(socket);
    this.joinRoom(socket, this.defaultRoom);
  },
  handleClientDisconnection: function (socket) {
    socket.on('disconnect', function() {
      var nameIndex = this.namesUsed.indexOf(this.nickNames[socket.id]);
      delete this.namesUsed[nameIndex];
      delete this.nickNames[socket.id];
    }.bind(this));
  },
  handleRoomJoining: function (socket) {
    socket.on('join', function(room) {
      debug('Handling Join for room', room);
      socket.leave(this.currentRoom[socket.id]);
      this.joinRoom(socket, room.newRoom);
    }.bind(this));
  },
  joinRoom: function (socket, room) {
    var usersInRoom;

    debug('joining room:', room);
    socket.join(room);
    this.currentRoom[socket.id] = room;
    socket.emit('joinResult', {room: room});

    socket.broadcast.to(room).emit('message.system', {
      content: this.nickNames[socket.id] + ' has joined ' + room + '.' });

// currently borked: https://github.com/Automattic/socket.io/pull/1630
//     usersInRoom = this.io.sockets.clients(room);
//
//     if (usersInRoom.lenth > 1) {
//       var usersInRoomSummary = 'Users currently in ' + room + ': ';
//       for (var index in usersInRoom) {
//         var userSocketId = usersInRoom[index].id;
//
//         if (userSocketId != socket.id) {
//           if (index > 0 ) {
//             usersInRoomSummary += ', ';
//           }
//           usersInRoomSummary += nickNames[userSocketId];
//         }
//       }
//       usersInRoomSummary += '.';
//       socket.emit('message', {content: usersInRoomSummary});
//     }
  }
});
