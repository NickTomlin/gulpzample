var _ = require('lodash');

module.exports = {
  user: _.template('<div class="message message--user"><span class="message__username"><%- username %><span class="message__divider">:</span> </span><span class="message__content"><%- message %></span></div>'),
  system: _.template('<div class="message message--system"><span class="message__content"><%- message %></span></div>'),
};

