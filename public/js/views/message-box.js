var $ = require('jquery');
var messageTemplate = require('../templates/messages');
var Backbone = require('backbone');
Backbone.$ = $;

module.exports = Backbone.View.extend({
  initialize: function () {
    this.listenTo(Backbone, 'messages.nameResult', this.handleNameResult);
    this.listenTo(Backbone, 'messages.user', this.handleUserMessage);
    this.listenTo(Backbone, 'messages.system', this.handleSystemMessage);
    this.listenTo(Backbone, 'room.join', this.handleJoinResult);
  },
  handleNameResult: function (result) {
    console.log('Handling name result');
    var message;
    if (result.success) {
      message = 'You are now known as ' + result.name + '.';
    } else {
      message = result.message;
    }

    this.$el.append(messageTemplate.system({message: message}));
  },
  handleSystemMessage: function (message) {
    console.log('MessageBox: System  message', message.content);
    this.renderMessage(messageTemplate.system({message: message.content}));
  },
  handleUserMessage: function (message) {
    console.log('MessageBox: User Message', message);
    this.renderMessage(messageTemplate.user({message: message.content, username: message.username}));
  },
  renderMessage: function (html) {
    this.$el.append(html);
  },
  handleJoinResult: function (result) {
    $('#room').text(result.room);
    this.renderMessage(messageTemplate.system({message: 'Room changed to ' + result.room }));
  }
});

